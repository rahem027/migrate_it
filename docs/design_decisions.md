# Plain python file vs upgrade-downgrade functions

Initially I planned on the python files being like such:
```python
print('upgrading...')
# do stuff
```

But while testing I found out that importing modules are cached
(and rightly so). So in my tests, I could not run the upgrading 
logic if some other test had already imported the module as code
above is for one time module initialization.

So, upgrade now looks like:
```python
def upgrade():
    print('upgrading')
    # do stuff
```

P.S. Initially I didnt think of downgrades. The idea was to add
a new migration that un-did the old one. But I had not thought
of what happens if upgrade throws. You probably want to downgrade
the version. So we now have a downgrade function as well that you 
need to define
