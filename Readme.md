# Migrate it

Migrate it is a migration tool. You can think of this as a general
purpose [https://alembic.sqlalchemy.org/en/latest/](alembic). The 
idea is we don't know anything about what you are migrating. This
allows you to migrate anything - configuration, database, firebase,
anything. 