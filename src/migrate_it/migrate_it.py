import logging
import os
import re
from importlib import import_module
from typing import Dict, Union, Set

logger = logging.getLogger(__name__)


class NoMigrationsToRun(Exception):
    pass


class MissingMigrations(Exception):
    missing_versions: Set[int]

    def __init__(self, missing_versions):
        self.missing_versions = missing_versions
        super().__init__(
            f'Versions: {missing_versions} are missing. Please create '
            f'a corresponding file named <version>[_comment].py for '
            f'each of the missing version. The file needs to start '
            f'with a positive integer indicating the version number '
            f'optionally followed by an _ and any comment and end '
            f'with .py. File must be a valid python module')


class CurrentVersionIsGreaterThanMaxVersion(Exception):
    current_version: int
    max_version: int

    def __init__(self, current_version, max_version):
        self.current_version = current_version
        self.max_version = max_version

        super().__init__(
            f'Current version: {current_version} is greater than '
            f'max version: {max_version}'
        )


class InvalidMigrationModules(Exception):
    def __init__(self, errors):
        self.errors = errors
        super().__init__(errors)


_pattern = re.compile('([0-9]+)(_.*)?.py')


def upgrade(migrations_package: Union[str, os.PathLike], current_version: int):
    # mapping of version (int) to module path (str)
    module: Dict[int, str] = {}
    for file in os.scandir(os.path.dirname(import_module(migrations_package).__file__)):
        # file.name is does not contain directory. Its just the file name
        match = _pattern.fullmatch(file.name)
        if not match:
            continue

        logger.debug(f'pattern matched: {file.name}')
        file_without_extension, _ = os.path.splitext(file.name)

        # group 0 is the entire expression. 1 is the first parenthesized expression,
        # 2 is the 2nd parenthesized expression and so on ....
        raw_version = match.group(1)
        module[int(raw_version)] = file_without_extension

    if len(module) == 0:
        raise NoMigrationsToRun('No migrations to run')

    versions = set(module.keys())
    max_version = max(versions)
    # We have 5 versions say. i.e. max_version is 5.
    # Theoretically they should be: {1, 2, 3, 4, 5}
    # Version needed is the set of numbers that should be there
    versions_needed = set(range(1, max_version + 1))
    # Versions which are in versions needed but not actually present.
    # in the directory. Say max versions is 5. But we only have
    # versions: {1, 3, 4, 5}. This means 2 is missing. Therefore,
    # we diff the two sets to display which files are missing
    missing_versions = versions_needed.difference(versions)
    if len(missing_versions) > 0:
        raise MissingMigrations(missing_versions)

    logger.info(f'Max migration version: {max_version}')

    logger.info(f'Current Version: {current_version}')

    if current_version > max_version:
        raise CurrentVersionIsGreaterThanMaxVersion(current_version, max_version)

    module_errors = {}
    for version in versions:
        module_name = f'{migrations_package}.{module[version]}'
        m = import_module(module_name)
        errors = module_errors.get(module_name, [])

        upgrade = getattr(m, 'upgrade', None)
        if not callable(upgrade):
            errors.append(f'{module_name} does not contain upgrade or upgrade is not a callable')

        downgrade = getattr(m, 'downgrade', None)
        if not callable(downgrade):
            errors.append(f'{module_name} does not contain downgrade or downgrade is not a callable')

        if len(errors) > 0:
            module_errors[module_name] = errors

    if len(module_errors) > 0:
        raise InvalidMigrationModules(module_errors)

    # We are one version 4 say. So start from version 5.
    # And range does not include the second parameter. So add 1
    # to ensure max_version is executed as well
    for version in range(current_version + 1, max_version + 1):
        module_name = f'{migrations_package}.{module[version]}'
        try:
            import_module(module_name).upgrade()
            logger.debug(f'imported module: {module_name}')
        except Exception as e:
            print(e)
            import_module(module_name).downgrade()
            raise


if __name__ == '__main__':
    upgrade('tests.success_test', 0)
