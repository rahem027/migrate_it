import os

from src.migrate_it.migrate_it import upgrade, NoMigrationsToRun, MissingMigrations, \
    CurrentVersionIsGreaterThanMaxVersion, \
    InvalidMigrationModules


def _remove_test_output_if_exists():
    try:
        os.remove('test_output.txt')
    except FileNotFoundError:
        pass


def directory_with_no_matching_file_names_raises_no_migrations_to_run():
    _remove_test_output_if_exists()

    try:
        upgrade('tests.no_migrations_test', 0)
        assert False
    except NoMigrationsToRun:
        pass


def directory_with_some_migration_versions_missing_raises_missing_migrations():
    _remove_test_output_if_exists()

    try:
        upgrade('tests.missing_versions_test', 0)
        assert False
    except MissingMigrations as e:
        # in the directory, 1 and 4 are present. 2 and 3 are missing
        assert e.missing_versions == {2, 3}


def passing_current_version_greater_than_the_highest_migration_number_in_the_directory_raises_current_version_is_greater_than_max_version():
    _remove_test_output_if_exists()

    try:
        # highest matching version is 3
        upgrade('tests.success_test', 4)
        assert False
    except CurrentVersionIsGreaterThanMaxVersion as e:
        assert e.current_version == 4
        assert e.max_version == 3


def ensure_all_migrations_have_upgrade_and_downgrade_functions():
    _remove_test_output_if_exists()

    try:
        upgrade('tests.ensure_upgrade_downgrade_functions_exists', 0)
        assert False
    except InvalidMigrationModules as e:
        assert len(e.errors) == 3

        assert e.errors['tests.ensure_upgrade_downgrade_functions_exists.1'] == [
            'tests.ensure_upgrade_downgrade_functions_exists.1 does not contain upgrade or upgrade is not a callable',
            'tests.ensure_upgrade_downgrade_functions_exists.1 does not contain downgrade or downgrade is not a callable',
        ]

        assert e.errors['tests.ensure_upgrade_downgrade_functions_exists.2'] == [
            'tests.ensure_upgrade_downgrade_functions_exists.2 does not contain downgrade or downgrade is not a callable',
        ]

        assert e.errors['tests.ensure_upgrade_downgrade_functions_exists.3'] == [
            'tests.ensure_upgrade_downgrade_functions_exists.3 does not contain upgrade or upgrade is not a callable',
        ]


def when_there_are_no_errors_all_the_matching_files_are_executed():
    _remove_test_output_if_exists()

    upgrade('tests.success_test', 0)

    with open('test_output.txt') as f:
        assert f.read() == '123'


def if_current_version_is_1_migrations_starting_from_2_are_executed():
    _remove_test_output_if_exists()

    upgrade('tests.success_test', 1)

    with open('test_output.txt') as f:
        assert f.read() == '23'


def downgrade_is_called_on_current_version_if_upgrade_throws_and_remaining_migrations_are_not_executed():
    _remove_test_output_if_exists()

    try:
        upgrade('tests.exceptions', 0)
        assert False
    except ValueError:
        pass
    except:
        assert False

    with open('test_output.txt') as f:
        read = f.read()
        assert read == '12error'


if __name__ == '__main__':
    directory_with_no_matching_file_names_raises_no_migrations_to_run()
    directory_with_some_migration_versions_missing_raises_missing_migrations()
    passing_current_version_greater_than_the_highest_migration_number_in_the_directory_raises_current_version_is_greater_than_max_version()
    ensure_all_migrations_have_upgrade_and_downgrade_functions()
    when_there_are_no_errors_all_the_matching_files_are_executed()
    if_current_version_is_1_migrations_starting_from_2_are_executed()
    downgrade_is_called_on_current_version_if_upgrade_throws_and_remaining_migrations_are_not_executed()
